package com.example.togglesettings;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.app.Activity;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.WindowManager;

public class MainActivity extends Activity {

	static boolean val = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		toggleMobileDataMethod1(true);
	    toggleMobileDataMethod2(false);
		
		toggleWifiSettings(true);
		setAppBrightness(0);

		setDeviceBrightness(0);
    	toggleAirPlaneMode(0);
	}
	
	
	public void setAppBrightness(int brightness){
		float brightnessF = brightness / (float)255;
		WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = brightnessF;
        getWindow().setAttributes(lp);
	}
	
	private void setDeviceBrightness(int brightness) {
		Settings.System.putInt(this.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
		Settings.System.putInt(this.getContentResolver(),
            Settings.System.SCREEN_BRIGHTNESS, brightness);
		
	}
	
	private void toggleAirPlaneMode(int toggleMode){
		Settings.Global.putString(getContentResolver(), "airplane_mode_on", String.valueOf(toggleMode));
	}
	
	public void toggleWifiSettings(boolean enableDisable){
	    WifiManager wiFiManager = ((WifiManager) getSystemService("wifi"));
    	wiFiManager.setWifiEnabled(enableDisable);
	}
	
	public void toggleMobileDataMethod1(boolean enableDisable){
	   try{
	      ConnectivityManager localConnectivityManager = (ConnectivityManager) this.getSystemService("connectivity");  //ConnectivityManagerDefinedState
	      Class localClass = localConnectivityManager.getClass();  // ConnectivityManagerClassState
	      Class[] arrayOfClass = new Class[1];  
	      arrayOfClass[0] = Boolean.TYPE;
	      Method localMethod = localClass.getMethod("setMobileDataEnabled", arrayOfClass); // SetMobileDataEnabledDefinedState for localMethod object.
	      Object[] arrayOfObject = new Object[1];    
	      arrayOfObject[0] = Boolean.valueOf(enableDisable);     
	      localMethod.invoke(localConnectivityManager, arrayOfObject); // SetMobileDataEnabledInvokedState
	    }
	    catch (Exception localException){
	    	localException.printStackTrace();
	    }
	}
	
	public void toggleMobileDataMethod2(boolean enableDisable){
		  ConnectivityManager localConnectivityManager = (ConnectivityManager) getSystemService("connectivity");
	    try{
	    	
	      Field localField = Class.forName(localConnectivityManager.getClass().getName()).getDeclaredField("mService"); //MServiceFieldDeclared1State
	      localField.setAccessible(true); //MServiceAccessible2State
	      Object localObject = localField.get(localConnectivityManager); //MServiceObject3State
	      Class localClass = Class.forName(localObject.getClass().getName()); //MServiceClass4State
	      Class[] arrayOfClass = new Class[1];
	      arrayOfClass[0] = Boolean.TYPE;
	      Method localMethod = localClass.getDeclaredMethod("setMobileDataEnabled", arrayOfClass);
	      Object[] arrayOfObject = new Object[1];
	      arrayOfObject[0] = Boolean.valueOf(enableDisable);
	      localMethod.invoke(localObject, arrayOfObject);
	      
	    }
	    catch (ClassNotFoundException localClassNotFoundException){
	        localClassNotFoundException.printStackTrace();
	        Log.d("TAGGGGGGGG", String.valueOf(localClassNotFoundException.getStackTrace()));
	    }
	    catch (NoSuchFieldException localNoSuchFieldException){
	        localNoSuchFieldException.printStackTrace();
	        Log.d("TAGGGGGGGG", String.valueOf(localNoSuchFieldException.getStackTrace()));
	    }
	    catch (SecurityException localSecurityException)
	    {
	        localSecurityException.printStackTrace();
	        Log.d("TAGGGGGGGG", String.valueOf(localSecurityException.getStackTrace()));
	    }
	    catch (NoSuchMethodException localNoSuchMethodException)
	    {
	        localNoSuchMethodException.printStackTrace();
	        Log.d("TAGGGGGGGG", String.valueOf(localNoSuchMethodException.getStackTrace()));
	    }
	    catch (IllegalArgumentException localIllegalArgumentException)
	    {
	        localIllegalArgumentException.printStackTrace();
	        Log.d("TAGGGGGGGG", String.valueOf(localIllegalArgumentException.getStackTrace()));
	    }
	    catch (IllegalAccessException localIllegalAccessException)
	    {
	        localIllegalAccessException.printStackTrace();
	        Log.d("TAGGGGGGGG", String.valueOf(localIllegalAccessException.getStackTrace()));
	    }
	    catch (InvocationTargetException localInvocationTargetException)
	    {
	        localInvocationTargetException.printStackTrace();
	        Log.d("TAGGGGGGGG", String.valueOf(localInvocationTargetException.getStackTrace()));
	    }
	}
	
}
