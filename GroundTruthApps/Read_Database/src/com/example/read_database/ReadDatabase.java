package com.example.read_database;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;



public class ReadDatabase extends Activity {
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        readSMSInbox();
    }
    
    public void readSMSInbox()
    {
    	Uri uri = Uri.parse("content://sms/inbox");
    	Cursor cursor = getContentResolver().query(uri, null, null, null, null);
    	
    	while(cursor.moveToNext())
    	{
    	   String msgData = "";
    	   for(int idx=0;idx<cursor.getColumnCount();idx++)
    	   {
    	       msgData += " " + cursor.getColumnName(idx) + ":" + cursor.getString(idx);
    	       Log.d("Message Data ", msgData );
    	   }
    	}
    }
}