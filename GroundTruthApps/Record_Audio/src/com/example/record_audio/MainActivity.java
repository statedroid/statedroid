package com.example.record_audio;

import java.io.File;
import java.io.IOException;

import android.Manifest.permission;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {

	static boolean val = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
	}

	public void onButtonAudioClicked(View v){
		RecordAudio();
	 }
	public void RecordAudio()
	{
		MediaRecorder recorder;
		File file1;
		File storageDir = Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_DOWNLOADS);

		if (getApplicationContext().checkCallingOrSelfPermission(permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
		{
			File repertoireStockage = Environment.getExternalStorageDirectory();
			try {
				file1 = File.createTempFile("RecordAudio",".mp4", storageDir);
	//			file1 =  Environment.getExternalStorageDirectory().getAbsoluteFile();
			} 
			catch (IOException e) {
				Log.e("RecordAudio", "I/O Probl em be f ore recording");
				Toast.makeText(getBaseContext(), "Exception again", Toast.LENGTH_LONG).show();
				e.printStackTrace();
				return;
			}
			recorder = new MediaRecorder();
			/*
			 *  If this method is not called, the output file will not contain an audio track. 
			 *  The source needs to be specified before setting recording-parameters or encoders.
			 *   Call this only before setOutputFormat().
			 */
			recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
			
			//Call this after setAudioSource()/setVideoSource() but before prepare().
			recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
	
			
			//If this method is not called, the output file will not contain an audio track. 
			//Call this after setOutputFormat() but before prepare().
			recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
			
			//Call this after setOutFormat() but before prepare()
			recorder.setMaxDuration(5000);
			try {
				// Call this after setOutputFormat() but before prepare().
				recorder.setOutputFile(file1.getCanonicalPath());
	
				//This method must be called after setting up the desired audio and video sources, encoders, file format, etc., but before start().
				// IllegalStateException	if it is called after start() or before setOutputFormat().
				recorder.prepare();
				
				//Call this after prepare().
				recorder.start();
	
				Thread.sleep(5000);
			}
			catch (IOException e) {
				e.printStackTrace();
			} 
			catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			recorder.stop();
			recorder.release();
		}
		
	}	
}
