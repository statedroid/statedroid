package com.example.answer_call;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.CallLog;
import android.telephony.TelephonyManager;

import com.android.internal.telephony.ITelephony;

public class CallReceiver extends BroadcastReceiver {
	Context context;

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		Bundle myBundle = intent.getExtras();
		if (myBundle != null) {
			System.out.println("--------Not null-----");
			try {
				if (intent.getAction().equals("android.intent.action.PHONE_STATE")) {
					String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
					System.out.println("--------in state-----");
					if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
						// Incoming call
						String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
						System.out.println("--------------my number---------" + incomingNumber);

						method1();
						method2();
					}
				}

			} catch (Exception ex) { // Many things can go wrong with reflection calls
				ex.printStackTrace();
			}
		}
	}

	private void method1() {
		try {
			TelephonyManager telephonymanager = (TelephonyManager) context.getSystemService("phone"); //TelephonyManagerDefinedState
			Method method = TelephonyManager.class.getDeclaredMethod("getITelephony", null);
			method.setAccessible(true); // GetITelephonyMethodSetAccessibleState
			ITelephony itelephony = ((ITelephony) method.invoke(telephonymanager, null)); // GetITelephonyMethodInvokedState 
			itelephony.answerRingingCall();
		} catch (SecurityException e) {
			
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			
			e.printStackTrace();
		} // GetITelephonyMethodDeclaredState for method object
		catch (IllegalArgumentException e) {
			
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			
			e.printStackTrace();
		} catch (RemoteException e) {
			
			e.printStackTrace();
		}

	}

	private void method2() {
		try {
			TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
			Class<?> classTelephony = Class.forName(telephonyManager.getClass().getName());
			Method methodGetITelephony = classTelephony.getDeclaredMethod("getITelephony");

			// Ignore that the method is supposed to be private                          
			methodGetITelephony.setAccessible(true);

			// Invoke getITelephony() to get the ITelephony interface
			Object telephonyInterface = methodGetITelephony.invoke(telephonyManager);
			Class<?> telephonyInterfaceClass = Class.forName(telephonyInterface.getClass().getName());
			Method methodEndCall = telephonyInterfaceClass.getDeclaredMethod("answerRingingCall");
			methodEndCall.invoke(telephonyInterface);

		} catch (SecurityException e) {
			
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			
			e.printStackTrace();
		} // GetITelephonyMethodDeclaredState for method object
		catch (IllegalArgumentException e) {
			
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		}
	}

}