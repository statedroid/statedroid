package com.example.block_call;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.CallLog;
import android.telephony.TelephonyManager;

import com.android.internal.telephony.ITelephony;

public class BlockCallReceiver extends BroadcastReceiver {

	/*
	 * This app contains block_call implemented in three different ways using
	 * method1, method2 and method3. Though phone is disconnected using call to first method1(), other two methods just show other ways of implementaiton.
	 * Moreover, this also contains deleteNumber() which deletes incoming caller number from the call logs database.
	 */

	Context context;

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		Bundle myBundle = intent.getExtras();
		if (myBundle != null) {
			System.out.println("--------Not null-----");
			try {
				if (intent.getAction().equals("android.intent.action.PHONE_STATE")) {
					String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
					System.out.println("--------in state-----");
					if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
						// Incoming call
						String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
						System.out.println("--------------my number---------" + incomingNumber);

						method1();
						method2();
						method3();
						
						deleteNumber(context, incomingNumber);

					}
				}

			} catch (Exception ex) { // Many things can go wrong with reflection calls
				ex.printStackTrace();
			}
		}
	}

	private void method1() {
		try {
			TelephonyManager telephonymanager = (TelephonyManager) context.getSystemService("phone"); //TelephonyManagerDefinedState
			Method method = TelephonyManager.class.getDeclaredMethod("getITelephony", null);
			method.setAccessible(true); // GetITelephonyMethodSetAccessibleState
			ITelephony itelephony = ((ITelephony) method.invoke(telephonymanager, null)); // GetITelephonyMethodInvokedState 
			itelephony.endCall(); //Call-ended
		} catch (SecurityException e) {
			
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			
			e.printStackTrace();
		} // GetITelephonyMethodDeclaredState for method object
		catch (IllegalArgumentException e) {
			
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			
			e.printStackTrace();
		} catch (RemoteException e) {
			
			e.printStackTrace();
		}

	}

	private void method2() {
		try {
			TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
			Class<?> classTelephony = Class.forName(telephonyManager.getClass().getName());
			Method methodGetITelephony = classTelephony.getDeclaredMethod("getITelephony");

			// Ignore that the method is supposed to be private                          
			methodGetITelephony.setAccessible(true);

			// Invoke getITelephony() to get the ITelephony interface
			Object telephonyInterface = methodGetITelephony.invoke(telephonyManager);
			Class<?> telephonyInterfaceClass = Class.forName(telephonyInterface.getClass().getName());
			Method methodEndCall = telephonyInterfaceClass.getDeclaredMethod("endCall");
			methodEndCall.invoke(telephonyInterface);

		} catch (SecurityException e) {
			
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			
			e.printStackTrace();
		} // GetITelephonyMethodDeclaredState for method object
		catch (IllegalArgumentException e) {
			
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		}
	}

	private void method3() {
		try {
			TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
			Class<?> classTelephony = Class.forName(telephonyManager.getClass().getName());
			Method methodGetITelephony = classTelephony.getDeclaredMethod("getITelephony");

			// Ignore that the method is supposed to be private                          
			methodGetITelephony.setAccessible(true);

			ITelephony telephonyService = (ITelephony) methodGetITelephony.invoke(telephonyManager);
			telephonyService.silenceRinger();
			telephonyService.endCall();

		} catch (SecurityException e) {
			
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			
			e.printStackTrace();
		} // GetITelephonyMethodDeclaredState for method object
		catch (IllegalArgumentException e) {
			
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		} catch (RemoteException e) {
			
			e.printStackTrace();
		}
	}

	private void deleteNumber(Context context, String number) {
		try {
			Uri CALLLOG_URI = Uri.parse("content://call_log/calls");

			context.getContentResolver().delete(CALLLOG_URI, CallLog.Calls.NUMBER + "=?", new String[] { number });
		} catch (Exception e) {
			e.getMessage();
		}
	}
}