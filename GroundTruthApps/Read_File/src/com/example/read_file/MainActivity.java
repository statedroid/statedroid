package com.example.read_file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.google.common.base.Charsets;
import com.google.common.io.Files;

public class MainActivity extends Activity {
	
	public void onCreate(Bundle state)
	{
		super.onCreate(state);
		setContentView(R.layout.activity_main);
		
		fileReading_apacheAPIs();
		fileReading_FileReader();
		fileReading_guavaAPIs();
		fileReading_InputStreamReader();
		fileReading_Scanner();
	}
		
	public void fileReading_Scanner(){
		List<Integer> integers = new ArrayList<Integer>();    
		Scanner fileScanner;
		try {
			fileScanner = new Scanner(new File("c:\\file.txt"));
			while (fileScanner.hasNextInt()){
				integers.add(fileScanner.nextInt());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Log.d("TAG", integers.toString());
	}
	
	public void fileReading_FileReader(){
		   String fileName="RAILWAY.txt";
       try{

          FileReader inputFile = new FileReader(fileName);
          BufferedReader bufferReader = new BufferedReader(inputFile);
          String line;
          String data = "";
          while ((line = bufferReader.readLine()) != null)   {
            data = line;
          }
          bufferReader.close();
		Log.d("TAG", data);
       }catch(Exception e){
          System.out.println("Error while reading file line by line:" + e.getMessage());                      
       }
	}

	public void fileReading_InputStreamReader(){
	      String string="";
        String file ="textFile.txt";

        //reading   
        try{
            InputStream ips=new FileInputStream(file); 
            InputStreamReader ipsr=new InputStreamReader(ips);
            BufferedReader br=new BufferedReader(ipsr);
            String line;
            while ((line=br.readLine())!=null){
                System.out.println(line);
                string+=line+"\n";
            }
            br.close(); 
            Log.d("TAG", string);
        }       
        catch (Exception e){
            System.out.println(e.toString());
        }
	}
	
	public void fileReading_guavaAPIs(){
		
		String text;
		try {
			text = Files.toString(new File("textfile.txt"), Charsets.UTF_8);
			Log.d("TAG", text);
			} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void fileReading_apacheAPIs(){
		String contents;
		try {
			contents = FileUtils.readFileToString(new File("path/to/your/file.txt"));
			String[] array = ArrayUtils.toArray(contents.split(" "));
			Log.d("TAG", Arrays.toString(array));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}

}
