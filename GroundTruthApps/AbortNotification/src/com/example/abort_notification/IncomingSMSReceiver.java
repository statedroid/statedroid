package com.example.abort_notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CallLog;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

public class IncomingSMSReceiver extends BroadcastReceiver {

	Context context;

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		Bundle myBundle = intent.getExtras();
		String action = intent.getAction();
		if (myBundle != null) {
			System.out.println("--------Not null-----");
			
			// TESTING for Abort notifications..
			abortBroadcast();
		}
	}
}