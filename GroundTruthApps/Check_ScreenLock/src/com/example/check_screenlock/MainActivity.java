package com.example.check_screenlock;


import android.app.Activity;
import android.app.KeyguardManager;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	public void onResume(){
		super.onResume();
		boolean isLockON = ((KeyguardManager)getSystemService("keyguard")).inKeyguardRestrictedInputMode();
		
		Log.d("MainActviityyyyyyyyyy", "LockON? = " + isLockON);
	}

}
