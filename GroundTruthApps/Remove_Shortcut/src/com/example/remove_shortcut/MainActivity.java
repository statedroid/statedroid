package com.example.remove_shortcut;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

/*
 * This app disable app icon by disabling its main activity or 
 * 
 * 
 * 
 */
public class MainActivity extends Activity {

	static boolean val = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
	}
	
	@Override
	protected void onResume(){
		super.onResume();

		//It gets hidden in both ways. For component, it needs a restart or may be, after a first tap on the app icon.
		method1();
		method2();

	}
	
	public void method1(){
		this.getPackageManager().setApplicationEnabledSetting(getApplicationContext().getPackageName(), 2, 1);
	}
	public void method2(){
		this.getPackageManager().setComponentEnabledSetting(new ComponentName(((Context)this), MainActivity.class), 2, 1);	
	}
	
}
