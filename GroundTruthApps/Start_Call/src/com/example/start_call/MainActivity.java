package com.example.start_call;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

public class MainActivity extends Activity {

	static boolean val = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		String telURI = "tel:" + "1234567890";
		Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(telURI));
		startActivity(intent);

	}
	
}
