package com.example.install_app;



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import android.app.Activity;
import android.os.Bundle;

public class MainActivity extends Activity {

	static boolean val = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		installApp();
	}
	private void installApp(){
		try {
		 Process process = Runtime.getRuntime().exec("/system/bin/pm install my.app");
		  BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		   int read;
		   
		    char[] buffer = new char[4096];
		    StringBuffer output = new StringBuffer();
		    while ((read = reader.read(buffer)) > 0) {
		        output.append(buffer, 0, read);
		    }
		    reader.close();
		    // Waits for the command to finish.
		    process.waitFor();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
