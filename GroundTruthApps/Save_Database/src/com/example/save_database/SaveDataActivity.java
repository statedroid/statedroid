package com.example.save_database;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Browser;
import android.telephony.TelephonyManager;
import android.util.Log;


public class SaveDataActivity extends Activity {
	
	FeedReaderDbHelper mDbHelper; 
	public static final String TABLE_NAME = "entry";
    public static final String COLUMN_NAME_TITLE = "title";
    public static final String COLUMN_NAME_SUBTITLE = "subtitle";
    public static final String _ID = "id";

	ContentValues values = new ContentValues();
	
	public void onCreate(Bundle instance){
		super.onCreate(instance);
		
		mDbHelper = new FeedReaderDbHelper(getApplicationContext());
		SQLiteDatabase db = mDbHelper.getWritableDatabase();
		
		TelephonyManager tmgr =(TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		String deviceID = tmgr.getDeviceId();
		
		values.put(COLUMN_NAME_TITLE, deviceID);
		values.put(COLUMN_NAME_SUBTITLE, "subtitle");
		
		// Insert the new row, returning the primary key value of the new row
		long newRowId = db.insert(TABLE_NAME, null, values);
		Log.d("TAGGG", "Row insertion IDD = " + newRowId);
		
		addBrowserBookmark();
		
	}
	
	private void addBrowserBookmark(){
		ContentValues values = new ContentValues();
		values.put(Browser.BookmarkColumns.TITLE, "Yahoo Inc.");
		values.put(Browser.BookmarkColumns.URL, "http://www.yahoo.com");
		values.put(Browser.BookmarkColumns.BOOKMARK, 1);
		values.put(Browser.BookmarkColumns.DATE, 0);
		
		// add bookmark to default browser              
		getContentResolver().insert(Browser.BOOKMARKS_URI, values);
		
		// add bookmark to Chrome
		Uri chromeUri = Uri.parse("content://com.android.chrome.browser/bookmarks");
		getContentResolver().insert(chromeUri,values);
	}

	
}