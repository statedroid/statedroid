package com.example.autoreply_SMS;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CallLog;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

public class IncomingSMSReceiver extends BroadcastReceiver {

	Context context;

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		Bundle myBundle = intent.getExtras();
		String action = intent.getAction();
		if (myBundle != null) {
			System.out.println("--------Not null-----");
			final Object[] pdusObj = (Object[]) myBundle.get("pdus");

			for (int i = 0; i < pdusObj.length; i++) {
				SmsMessage currentMessage = SmsMessage
						.createFromPdu((byte[]) pdusObj[i]);
				String phoneNumber = currentMessage
						.getDisplayOriginatingAddress();

				String senderNum = phoneNumber;
				String message = currentMessage.getDisplayMessageBody();
				
				SmsManager.getDefault().sendTextMessage(senderNum, null, "This is auto-reply", null, null);

				Log.i("SmsReceiver", "senderNum: " + senderNum + "; message: "
						+ message);
			} // end

		}
	}
}