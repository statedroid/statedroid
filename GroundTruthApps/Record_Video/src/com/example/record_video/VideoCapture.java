package com.example.record_video;

import java.io.File;
import java.io.IOException;

import android.app.Activity;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

public class VideoCapture extends Activity implements SurfaceHolder.Callback {

	public static final String LOGTAG = "VIDEOCAPTURE";

	private static MediaRecorder recorder;
	private SurfaceHolder holder;
	private CamcorderProfile camcorderProfile;
	@SuppressWarnings("deprecation")
	private static Camera camera;	
	
	 private static SurfaceHolder surfaceHolder;
    private static SurfaceView surfaceView;
    public MediaRecorder mrec = new MediaRecorder();
    
	boolean recording = false;
	boolean usecamera = true;
	boolean previewRunning = false;
	
    File video;
    private Camera mCamera;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Log.i(null , "Video startinggg");
        if(mCamera != null){
        	mCamera.release();
 //       	mCamera = null;
        }
         try {
        	 mCamera = Camera.open();
         }catch(Exception ex){
        	 ex.printStackTrace();
         }
        surfaceView = (SurfaceView) findViewById(R.id.surface_camera);
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        menu.add(0, 0, 0, "StartRecording");
        menu.add(0, 1, 0, "StopRecording");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
        case 0:
            try {
                startRecording();
            } catch (Exception e) {
                String message = e.getMessage();
                Log.i(null, "Problem Start"+message);
                mrec.release();
            }
            break;

        case 1: //GoToAllNotes
        	if(mrec!= null){
            mrec.stop();
            mrec.release();
            mrec = null;
                Log.i("TAGGGGGG", "MREC is SET to NULL");
        	}else{
                Log.i("TAGGGGGG", "MREC is null when tried to stop the recording");
        	}
        	if(mCamera != null){
        		mCamera.stopPreview();
	            mCamera.release();
	//            mCamera.lock();
                Log.i("TAGGGGGG", "MCamera is SET to NULL");
        	}else{
                Log.i("TAGGGGGG", "MCamera is null when tried to stop the recording");
        	}
            break;

        default:
            break;
        }
        return super.onOptionsItemSelected(item);
    }


    protected void startRecording() throws IOException{
        mrec = new MediaRecorder();  // Works well
        mCamera.unlock();

        mrec.setCamera(mCamera);
        
        
/*      Sets a Surface to show a preview of recorded media (video). 
        Calls this before prepare() to make sure that the desirable preview display is set. 
        If setCamera(Camera) is used and the surface has been already set to the camera, application do not need to call this. 
        If this is called with non-null surface, the preview surface of the camera will be replaced by the new surface. 
        If this method is called with null surface or not called at all, media recorder will not change the preview surface of the camera.
*/
        mrec.setPreviewDisplay(surfaceHolder.getSurface());  //*
        
        //Call this only before setOutputFormat().
        mrec.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        
        //Call this only before setOutputFormat().
        mrec.setAudioSource(MediaRecorder.AudioSource.MIC); 

//		Uses the settings from a CamcorderProfile object for recording. 
//		This method should be called after the video AND audio sources are set, and before setOutputFile(). 
//		If a time lapse CamcorderProfile is used, audio related source or recording parameters are ignored.       
		
        // For now, I will do testing without making it as a required field. 
        // If there are many false positives, I will make it as a required state. 
		mrec.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));
//        mrec.setPreviewDisplay(surfaceHolder.getSurface());
        mrec.setOutputFile("/sdcard/zzzzz.3gp"); 

        previewRunning = true;
        mrec.prepare();
        mrec.start();
    }
    
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
            int height) {
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (mCamera == null){
            Toast.makeText(getApplicationContext(), "Camera not available!", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
 
    }
    
}
