package com.example.ringer_normal;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.os.Bundle;

public class MainActivity extends Activity {

	static boolean val = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		AudioManager audiomanager =(AudioManager) getSystemService(Context.AUDIO_SERVICE);
		audiomanager.setRingerMode(2);
	}
}
