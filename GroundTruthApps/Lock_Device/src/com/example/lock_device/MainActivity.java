package com.example.lock_device;

import com.example.phonecaller.R;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.os.Bundle;

public class MainActivity extends Activity {

	static boolean val = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		doLockScreenProgrammatically(getApplicationContext());
	}
	
	public void doLockScreenProgrammatically(Context pkg){
		DevicePolicyManager devicePolicyManager =(DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
		ComponentName demoDeviceAdmin =new ComponentName(pkg, MainActivity.class);

		devicePolicyManager.setPasswordQuality(demoDeviceAdmin,DevicePolicyManager.PASSWORD_QUALITY_UNSPECIFIED);
		devicePolicyManager.setPasswordMinimumLength(demoDeviceAdmin, 5);
		
		boolean result = devicePolicyManager.resetPassword("123456", DevicePolicyManager.RESET_PASSWORD_REQUIRE_ENTRY);
	}

}
