# README #

This repository contains two items:
	1- source code of StateDroid framework
	2- 16 testbed apps from our ground truth dataset.

### StateDroid framework ###

* Current uploaded source code contains prototype implementation which is not very optimized for high performance. 
* We are working on its development, and will extend and optimize this version over the time.