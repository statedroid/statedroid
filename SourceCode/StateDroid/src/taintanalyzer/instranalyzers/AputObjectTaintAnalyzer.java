package taintanalyzer.instranalyzers;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Random;

import models.cfg.InstructionResponse;
import models.cfg.Register;
import models.symboltable.EntryDetails;
import models.symboltable.SourceInfo;
import models.symboltable.SymbolSpace;
import models.symboltable.SymbolTableEntry;

import org.apache.log4j.Logger;

import taintanalyzer.TaintAnalyzer;
import configuration.Config;

public class AputObjectTaintAnalyzer  extends BaseTaintAnalyzer{

	
	private InstructionResponse ir;
	private SymbolSpace localSymSpace;
    boolean tainted=false;
    String[] used ;
    String changed;
    SymbolTableEntry destLocalEntry;
    SymbolTableEntry srcLocalEntry;
    SymbolTableEntry indexLocalEntry;
    SymbolTableEntry destGlobalEntry;
    SymbolTableEntry srcGlobalEntry;
    Register srcReg;
    Register destReg;
    Register indexReg;

	public AputObjectTaintAnalyzer(TaintAnalyzer ta )
	{
		this.ir = ta.getIr();
		this.localSymSpace = Config.getInstance().getLocalSymbolSpace();
		 logger = Logger.getLogger(AputObjectTaintAnalyzer.class);			
	}
//	// 		0x74 aput-object v0, v5, v1   ==> v0=source, v5=destination, v1= index
	
//What to do with the storing values. One aput can make destination tainted and other can make untainted. What to do? Store entries in recordFieldList
	// and then use those entries in aget- analyzer also. Since index values are not known, storing and retriveing each entry fromr recordlist
	//will produce the same accuracy as marking the recording entry as tainted.
	
	
	//Edit1: Keep storing new array-elements into the array's recordFieldList. If any of the elements is tainted, mark the whole
	//array as tainted. So even if array is passed as a parameter to some other function, output of non-handled api calls will be
	// marked as tainted. However, when getting back
	
	//Edit2: No need to store all the elements. When new element is tainted, just mark the array as tainted, and add SourceInfo object
	// from new element to the list of array-object. Similarly, when getting back (aget), if array is tainted, give back all
	// SourceInfo elements because any of the index value can be tainted. 
	//:: Problem with this approach is that once an array is tainted, it is always tainted.
	///
	
	//Edit3: Going to store elements in the recordFieldList. It may not work in many cases, but it can in some cases.
	// It will help in straight-forward cases where we just want to get elements of the array.
	
	//Edit4: I am going to mark the whole array as tainted if any of the index or the src entry is tainted. I will also mark it as record= false onwards.
	// No point of storing in recordFieldList.
	public Object analyzeInstruction()
	{
		   srcReg = ir.getInvolvedRegisters().get(0);
		   destReg = ir.getInvolvedRegisters().get(1);
		   indexReg = ir.getInvolvedRegisters().get(2);
		   
	       srcLocalEntry = localSymSpace.find(srcReg.getName());
	       destLocalEntry = localSymSpace.find(destReg.getName());
	       indexLocalEntry= localSymSpace.find(indexReg.getName());
	       
	       Hashtable immutableObjects = Config.getInstance().getImmutableObjects();
	       
	       //How to know if destEntry is to be created a new one or used existing one.
	       // Only way is to handle all aput-instructions individually and set their types accordingly. 
	       // So, first check if existing entry and current entry have same types, go with the existing one, else
	       // create a new entry. 
	       
	       // A function can return an array of any type. so need to fix it laters.
	       // a new array analyzer should create an array and specify its type. And that type should be checked here.
	       if(srcLocalEntry != null)
	       {
	    	   if( destLocalEntry != null)
	    	   {
	    		   EntryDetails destEntryDetails = destLocalEntry.getEntryDetails();
	    		   
	    		   destEntryDetails.setValue(" ");

	    		  // destEntryDetails.setType(ir.getReturnType());   // Lcom/test/maliciousactivity/User;	
	    		   destLocalEntry.setLineNumber(ir.getLineNumber());
	    		   
	    		   ArrayList<SourceInfo> srcInfoList = destEntryDetails.getSourceInfoList();
	    		   
	    		   if(indexLocalEntry != null)
	    		   {
			     	   if(indexLocalEntry.getEntryDetails().isTainted())
			     	   {
	    				   destEntryDetails.setTainted(true);

	    				   ArrayList<SourceInfo> existingSilist = indexLocalEntry.getEntryDetails().getSourceInfoList();
	    				   
	    				   if(existingSilist != null && existingSilist.size() >0)
	    				   {
	    					   if(srcInfoList == null)
	    						   srcInfoList = new ArrayList<SourceInfo>();
							   for(SourceInfo si: existingSilist )
							   {
								   if(!srcInfoList.contains(si))
								   {
									   srcInfoList.add(si);
								   }
							   }
	    				   }
						   
			     	   }
	    		   }
	    		   
    			   if(srcLocalEntry.getEntryDetails().isTainted())
    			   {
    				   destEntryDetails.setTainted(true);				     	   
    				   ArrayList<SourceInfo> existingSilist = srcLocalEntry.getEntryDetails().getSourceInfoList();
    				   
    				   if(existingSilist != null && existingSilist.size() >0)
    				   {
    					   if(srcInfoList == null)
    						   srcInfoList = new ArrayList<SourceInfo>();
						   for(SourceInfo si: existingSilist )
						   {
							   if(!srcInfoList.contains(si))
							   {
								   srcInfoList.add(si);
							   }
						   }
    				   }
		    		   
//		    		   if(!destEntryDetails.isConstant()) // If already not true
//		    			   destEntryDetails.setConstant(srcLocalEntry.getEntryDetails().isConstant());

    			   }
 
    			   destEntryDetails.setSourceInfoList(srcInfoList);
	    		   destEntryDetails.setField(false);

	    		   destEntryDetails.setRecord(false);
	    		   
	    		   destLocalEntry.setEntryDetails(destEntryDetails);

	    		   

	    	   }
    		   else
    		   {

    			   destLocalEntry = new SymbolTableEntry();
	    		   EntryDetails destEntryDetails = destLocalEntry.getEntryDetails();
	    		   
	    		   destEntryDetails.setValue(" ");

	    		   destEntryDetails.setType(ir.getReturnType());   // Lcom/test/maliciousactivity/User;	
	    		   destLocalEntry.setLineNumber(ir.getLineNumber());
	    		   
	    		   ArrayList<SourceInfo> srcInfoList = destEntryDetails.getSourceInfoList();
	    		   
	    		   if(indexLocalEntry != null)
	    		   {
			     	   if(indexLocalEntry.getEntryDetails().isTainted())
			     	   {
	    				   destEntryDetails.setTainted(true);

	    				   ArrayList<SourceInfo> existingSilist = indexLocalEntry.getEntryDetails().getSourceInfoList();
	    				   
	    				   if(existingSilist != null && existingSilist.size() >0)
	    				   {
	    					   if(srcInfoList == null)
	    						   srcInfoList = new ArrayList<SourceInfo>();
							   for(SourceInfo si: existingSilist )
							   {
								   if(!srcInfoList.contains(si))
								   {
									   srcInfoList.add(si);
								   }
							   }
	    				   }
						   
			     	   }
	    		   }
	    		   
    			   if(srcLocalEntry.getEntryDetails().isTainted())
    			   {
    				   destEntryDetails.setTainted(true);				     	   
    				   ArrayList<SourceInfo> existingSilist = srcLocalEntry.getEntryDetails().getSourceInfoList();
    				   
    				   if(existingSilist != null && existingSilist.size() >0)
    				   {
    					   if(srcInfoList == null)
    						   srcInfoList = new ArrayList<SourceInfo>();
						   for(SourceInfo si: existingSilist )
						   {
							   if(!srcInfoList.contains(si))
							   {
								   srcInfoList.add(si);
							   }
						   }
    				   }
		    		   
//		    		   if(!destEntryDetails.isConstant()) // If already not true
//		    			   destEntryDetails.setConstant(srcLocalEntry.getEntryDetails().isConstant());

    			   }

    			   destEntryDetails.setSourceInfoList(srcInfoList);
	    		   destEntryDetails.setField(false);

	    		   destEntryDetails.setRecord(false);
	    		   
	    		   destLocalEntry.setEntryDetails(destEntryDetails);

	    		   localSymSpace.addEntry(destLocalEntry);
		     	   
    		   }
	       }
	    

	       logger.debug("\n AputTaintAnalyzer");
	       localSymSpace.logInfoSymbolSpace();
	       
	       return null;
	}
	

}

