package taintanalyzer.instranalyzers;
//package taintanalyzer;
//
//
//import java.util.ArrayList;
//
//import org.apache.log4j.Logger;
//
//import configuration.Config;
//
//import symboltable.EntryDetails;
//import symboltable.SourceInfo;
//import symboltable.SymbolSpace;
//import symboltable.SymbolTableEntry;
//import cfg.InstructionResponse;
//import cfg.Register;
//
public class DefaultTaintAnalyzer  extends BaseTaintAnalyzer{
//
//	
//	private InstructionResponse ir;
//	private SymbolSpace localSymSpace;
//    boolean tainted=false;
//    String[] used ;
//    String changed;
//    ArrayList<Register> involvedRegisters;
//    SymbolTableEntry returnEntry ;
//
//
//    // This class handles all API calls which have no specified handler.
//    // It assumes that if input is tainted, output is tainted.
//
//	public DefaultTaintAnalyzer(TaintAnalyzer ta )
//	{
//		this.ir = ta.getIr();
//		this.localSymSpace = Config.getInstance().getLocalSymbolSpace();
//		involvedRegisters = new ArrayList<Register>();
//		returnEntry = new SymbolTableEntry();
//		 logger = Logger.getLogger(DefaultTaintAnalyzer.class);			
//	}
//	
//	public Object analyzeInstruction()
//	{
// 	   		
//		involvedRegisters = ir.getInvolvedRegisters();
//     	   
//	   String instrTypeBySyntax = ir.getInstr().getTypeBySyntax();
//	   
//	   		
//		   boolean tainted = false;
//		   SymbolTableEntry entry = null;
//		   String sourceAPI = "";
//		   SymbolTableEntry destEntry= null;
//		   ArrayList<String> srcAPIList = new ArrayList<String>();
//		   
//		   // 0x4e invoke-direct v5, v3, Ljava/net/URL;-><init>(Ljava/lang/String;)V
//		   // In case of invoke-direct, we almost always store result in the first entry
//		   
//		   if(instrTypeBySyntax.equalsIgnoreCase("invoke-direct"))
//		   {
//			   if(involvedRegisters.size() > 0)
//			   {
//				   for(int i=1; i < involvedRegisters.size(); i++)
//				   {
//					   Register reg = involvedRegisters.get(i);
//					   
//					   entry = localSymSpace.find(reg.getName());
//					   
//					   if(entry != null)
//					   {
//						   if(entry.isTainted())
//						   {
//							   tainted = true;
//							   if(! sourceAPI.isEmpty())
//								   sourceAPI += "," + entry.getSourceAPI() ;
//							   else
//								   sourceAPI = entry.getSourceAPI() ;
//						   }
//					   }
//				   }
//				   
//				   Register destReg = involvedRegisters.get(0);
//				   destEntry = localSymSpace.find(destReg.getName());
//				   
//			       if(destEntry != null)
//			       {
//			           EntryDetails destEntryDetails = destEntry.getEntryDetails();
//			           ArrayList<SourceInfo> srcInfoList = destEntry.getEntryDetails().getSourceInfoList();
//	
//			    	   
//			    	   destEntry.setName(destReg.getName());
//			    	   destEntry.setType(destReg.getType());
//			    	   destEntry.setLineNumber(ir.getLineNumber());
//			    	   destEntry.setConstant(false);
//			    	   destEntry.setField(false);
//			    	   destEntry.setRecord(false); 
//			    	   
//			    	   if(tainted)
//			    	   {
//				    	   destEntry.setTainted(tainted);
//				    	   
//				    	   if(entry != null && entry.getValue() != null)
//				    	   {
//					    	   destEntry.setValue(entry.getValue());
//					    	   destEntry.setInstrInfo(entry.getInstrInfo());
//				    	   }
//				    	   
//				    	   if(destEntry.isTainted())
//				    		   destEntry.setSourceAPI(sourceAPI+destEntry.getSourceAPI());
//			    	   }
//			//This else condition is adjusted above. If dest is tainted, it will remain tainted or untained if "tainted" value is false.
//	//		    	   else if(destEntry.isTainted())
//	//		    	   {
//	//			    	   destEntry.setTainted(true);
//	//			    	   destEntry.setValue(entry.getValue());
//	//			    	   destEntry.setInstrInfo(entry.getInstrInfo());
//	//			    	   destEntry.setSourceAPI(sourceAPI);
//	//
//	//		    	   }
//			       }
//			       else
//			       {
//			    	   destEntry = new SymbolTableEntry();
//		
//			    	   destEntry.setName(destReg.getName());
//			    	   destEntry.setType(destReg.getType());
//			    	   destEntry.setLineNumber(ir.getLineNumber());
//			    	   destEntry.setConstant(false);
//			    	   destEntry.setField(false);
//			    	   destEntry.setRecord(false); 
//			    	   
//		    		   destEntry.setTainted(tainted);
//			    	   
//			    	   if(tainted)
//			    	   {
//				    	   if(entry != null && entry.getValue() != null)
//				    	   {
//					    	   destEntry.setValue(entry.getValue());
//					    	   destEntry.setInstrInfo(entry.getInstrInfo());
//				    	   }
//				    	   destEntry.setSourceAPI(sourceAPI);
//			    		   
//			    	   }
//			    	   localSymSpace.addEntry(destEntry);
//			    	   
//			       }
//			   }
//		   else
//		   {
//			   destEntry = new SymbolTableEntry();
//			   
//			   destEntry.setType(ir.getReturnType());
////	    	   destEntry.setName(destReg.getName());
////	    	   destEntry.setType(destReg.getType());
//	    	   destEntry.setLineNumber(ir.getLineNumber());
//	    	   destEntry.setConstant(false);
//	    	   destEntry.setField(false);
//	    	   destEntry.setRecord(false); 
//	    	   destEntry.setTainted(false);
//
//	    	   destEntry.setValue("");
//	    	   destEntry.setInstrInfo("");
//	    	   destEntry.setSourceAPI("");
//	    		   
//	    	   
//		   }
//
//	   		logger.debug("\n DefaultTaintAnalyzer");
//	       localSymSpace.logInfoSymbolSpace();
//	       return destEntry;
//	}
}
