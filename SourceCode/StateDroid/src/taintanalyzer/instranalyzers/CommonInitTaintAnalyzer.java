package taintanalyzer.instranalyzers;
//package taintanalyzer;
//
//import java.util.ArrayList;
//
//import org.apache.log4j.Logger;
//
//import configuration.Config;
//
//
//import symboltable.EntryDetails;
//import symboltable.SourceInfo;
//import symboltable.SymbolSpace;
//import symboltable.SymbolTableEntry;
//import taintanalyzer.BaseTaintAnalyzer;
//import taintanalyzer.TaintAnalyzer;
//import cfg.Instruction;
//import cfg.InstructionResponse;
//import cfg.Register;
//
public class CommonInitTaintAnalyzer extends BaseTaintAnalyzer{
//
//	private InstructionResponse ir;
//	private Instruction instr;
//	private TaintAnalyzer ta;
//	private SymbolSpace localSymSpace;
//
//	public CommonInitTaintAnalyzer(TaintAnalyzer ta)
//	{
//		ir = ta.getIr();
//		instr = ta.getIr().getInstr();
//		this.localSymSpace = Config.getInstance().getLocalSymbolSpace();	
//		logger = Logger.getLogger(CommonInitTaintAnalyzer.class);
//	}
//
//	   // This class handles multiple type of <init> calls.
//    /* 		0x114 invoke-direct v1, v2, Landroid/content/Intent;-><init>(Ljava/lang/String;)V
//     * 		0x0 invoke-direct v0, Landroid/content/BroadcastReceiver;-><init>()V
//     * 		0x1a invoke-direct v4, v5, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V
//     * 		0x4 invoke-direct v0, Landroid/os/Handler;-><init>()V
//     * 		0xb2 invoke-direct v1, v0, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V
//     * 		0x9a invoke-direct v0, v1, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V
//     * 		0x1c invoke-direct v1, v0, Ljava/lang/Long;-><init>(Ljava/lang/String;)V
//     * 		0xb8 invoke-direct v6, v7, v8, v9, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String; I Ljava/lang/String;)V
//     * 		0xd2 invoke-direct v3, Landroid/content/ContentValues;-><init>()V
//     * 		0x66 invoke-direct v0, Lorg/json/JSONObject;-><init>()V
//     * 
//     */
//	public Object analyzeInstruction()
//	{
//
//		Register destReg = ir.getInvolvedRegisters().get(0);
//     	   
//        SymbolTableEntry entry = localSymSpace.find(destReg.getName());
//
//        String regType = ir.getInstr().getCurrPkgClassName();
//
//        
//        if(entry != null)
//        {
//        	// If entry is not null, it should get type and name for new-instance instruction output.
//            EntryDetails destEntryDetails = destEntry.getEntryDetails();
//            ArrayList<SourceInfo> srcInfoList = destEntry.getEntryDetails().getSourceInfoList();
//
////      	entry.setName(destReg.getName());
////     	    entry.setType(regType);
//    	    entry.setLineNumber(ir.getLineNumber());
//    	    entry.setConstant(false);
//    	    entry.setField(false);
//    	    
//    	    
//    	    entry.setTainted(false);
//    	    entry.setRecord(false);
//    	   
////    	    entry.setValue(destReg.getValue());
//    	    entry.setSourceAPI("");
//    	    entry.setInstrInfo("");
//       }
//       else
//       {
//    	   entry = new SymbolTableEntry();
//
//     	    entry.setName(destReg.getName());
//    	    entry.setType(regType);
//	   	    entry.setLineNumber(ir.getLineNumber());
//	   	    entry.setTainted(false);
//	   	    entry.setConstant(false);
//	   	    entry.setField(false);
//	   	    entry.setRecord(false);
//	   	   
////   	    entry.setValue(destReg.getValue());
//	   	    entry.setSourceAPI("");
//	   	    entry.setInstrInfo("");
//    	    
//	   	    localSymSpace.addEntry(entry);
//    	   
//       }
//        
//	       logger.debug("\n InitAnalyzer");
////	       localSymSpace.printSymbolSpace();
//	       return null;
//	}
}
