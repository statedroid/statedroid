package models.symboltable;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Stack;

public class ContextStack {

	
	 
		private Stack entries = null ;
		
		public ContextStack()
		{
			entries = new Stack();
		}
	   
	   public void saveContext(Context contextObj) {
		      entries.push(contextObj);
		   }

//	   public Context retrieveContext() 
//	   {
//		   Context context = new Context();
//		   context = (Context) entries.peek();
//		   
//		   
//	      return  context.getNewCopy(context);
//	   }
	   
	   public Context retrieveContext() 
	   {
//		   Context context = new Context();
//		   context = (Context) entries.peek();
		   
	      return  (Context) entries.pop();
	   }
	   
	   public Context retrieveContextByPeek() 
	   {
		   Context currContext = null;
		   
		   if( entries!= null && entries.size() > 0)
			   currContext = (Context) entries.peek(); 
	      return  currContext;
	   }

}
