package models.cfg;
import iterator.CommonIterator;

import java.util.ArrayList;
import java.util.Iterator;

import analyzer.Analyzer;


public class InterProcCFG extends CFGComponent implements Iterable<CFGComponent> {

//	private ArrayList<CFGComponent> compCollection;

	public InterProcCFG()
	{
		compCollection = new ArrayList<CFGComponent>();
	}

	
	public void addItem(CFGComponent comp)
	{
		compCollection.add(comp);
	}

	public boolean removeItem(CFGComponent comp)
	{
		compCollection.remove(comp);
		return true;
	}

//	@Override
	public Iterator iterator() {
//		Iterator iterator = instrList.iterator();
		return (Iterator) new CommonIterator(this);
	}

	public void accept(Analyzer a)
	{
           a.analyze(this);
	}
	public CFG getMethodBySignature(MethodSignature ms)
	{
		CFG returnObj = null;
		// for loop returns CFG item. You can write some logic here to get the method object
		// by signature by iterating over different activities.
		
		// For POC, I am getting User<init> method here.
		
		ClassObj classObj = (ClassObj) compCollection.get(0);
		
		Iterator it = (Iterator) classObj.iterator();
		while(it.hasNext())
		{
			CFG cfg = (CFG) it.next();
			if(cfg.getKey().equalsIgnoreCase("cfg2"))
			{
				returnObj = cfg;
				break;
			}
		}
		
		return returnObj;
		
	}
	
	public void setItem(int index, CFGComponent comp)
	{
		compCollection.set(index, comp);
	}

	public BasicBlock getBasicBlockByKey(String key)
	{
		
		
		return null;
	}
//	public void setAnalayzerType(Analyzer a)
//	{
//		this.analyzer = a;
//		
//	}
	public String getKey()
	{
		return this.key;
	}
	public void setKey(String key)
	{
		this.key = key;
	}
}
