package models.cfg;

public class MethodResponse {

	private BasicBlock lastBBResponse;

	public BasicBlock getLastBBResponse() {
		return lastBBResponse;
	}

	public void setLastBBResponse(BasicBlock lastBBResponse) {
		this.lastBBResponse = lastBBResponse;
	}


}
