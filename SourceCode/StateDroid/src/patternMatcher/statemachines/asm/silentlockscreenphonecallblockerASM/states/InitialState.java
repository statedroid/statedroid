package patternMatcher.statemachines.asm.silentlockscreenphonecallblockerASM.states;
import patternMatcher.events.asm.phonevolume.RingerModeSilentASMEvent;
import patternMatcher.events.asm.phonevolume.VoiceCallStreamDecreasingASMEvent;
import patternMatcher.statemachines.State;
import patternMatcher.statemachines.asm.silentlockscreenphonecallblockerASM.SilentLockScreenPhoneCallBlockerASMStates;
import taintanalyzer.TaintAnalyzer;

public class InitialState extends SilentLockScreenPhoneCallBlockerASMStates{

	private String currInstr="";
	private TaintAnalyzer ta;
	public InitialState(){}
	public InitialState(TaintAnalyzer ta){
		this.ta = ta;
	}
	
	/*   Attack definition
	 * 
	(1) RingerModeSilentState ==> PhoneCallBlockerState ==> RingerModeNormalState (ATTACK reported here) --------------> silentPhoneCallBlockerASM
	(2) Lock phone screen ==> Start a phone call ==> End phone call  -------------> lockscreenphonecallerASM
	(3) Intercept phone call ==> Set phone ringer to silent mode ==> Lock phone screen ==> block phone call ==> restore phone volume ----------> Current
	(4) Intercept phone call ==> Lock phone screen ==> Set phone ringer to silent mode ==> block phone call ==> restore phone volume
		 
	*/	

	@Override
	public State update(RingerModeSilentASMEvent e) {
		return (State) new RingerModeSilentState(this.ta);
	}

	//Both kind-of achieve the same thing.
	@Override
	public State update(VoiceCallStreamDecreasingASMEvent e) {
		return (State) new RingerModeSilentState(this.ta);
	}
}
