package patternMatcher.statemachines.asm.filereaderASM.states;
import patternMatcher.attackreporter.GenericReport;
import patternMatcher.events.asm.FileReaderASMEvent;
import patternMatcher.statemachines.State;
import patternMatcher.statemachines.asm.filereaderASM.FileReaderASMStates;
import taintanalyzer.TaintAnalyzer;
import configuration.Config;


public class InitialState extends FileReaderASMStates{

	private TaintAnalyzer ta;
	public InitialState(TaintAnalyzer taParam){
		this.ta = taParam;
	}
	public InitialState(){}
	
	//NEVER USED FOR NOW BUT MAY BE USED IN FUTURE.

	@Override
	public State update(FileReaderASMEvent e) {
		String permStr = Config.getInstance().getCurrCFGPermutationString();

		GenericReport rep = new GenericReport();
		rep.setInstrContainerCls(e.getCurrPkgClsName());
		rep.setInstContainerMthd(e.getCurrMethodName());
		rep.setCompPkgName(e.getCurrComponentPkgName());
		rep.setCompCallbackMethdName(e.getCurrCompCallbackMethodName());
 		rep.setCurrComponentClsName(e.getCurrComponentName());
 		rep.setInstrContainerCls(e.getCurrPkgClsName());
 		rep.setInstContainerMthd(e.getCurrMethodName());
 		rep.setPermutationStr(permStr);
 		
// 		SymbolTableEntry streamTypeEntry = (SymbolTableEntry) e.getEventInfo().get("streamType");
// 		SymbolTableEntry streamActionEntry = (SymbolTableEntry) e.getEventInfo().get("streamAction");
// 		
// 		String streamType = streamTypeEntry.getEntryDetails().getValue();
// 		String streamAction = streamActionEntry.getEntryDetails().getValue();
// 		
// 		String msg = "##### This app can modify the volume settings by \"" + streamAction + "\" of stream \"" + streamType + "\".\n";
// 		rep.setMessage(msg);
// 		
// 		InstructionResponse ir = (InstructionResponse) e.getEventInfo().get(InstructionResponse.CLASS_NAME);
// 	    rep.setSinkAPI(ir.getInstr().getText());
// 	    
// 		if(!AttackReporter.getInstance().checkIfAudioManagerStreamVolumeChangedReportExists(rep))
// 		{
// 			AttackReporter.getInstance().getAudioManagerStreamVolumeChangedReportList().add(rep);
// 			rep.printReport();
// 		}
 		
		return this;
	}

}
