package iterator;
import java.util.ArrayList;

import java.util.Iterator;

import models.cfg.BasicBlock;
import models.cfg.CFGComponent;


public class ClassObjIterator implements Iterator<Object>{

	private CFGComponent currComp;
	private int currPosition;
	private ArrayList<CFGComponent> currClassObjCollection;
	
	public ClassObjIterator(CFGComponent myComp)
	{
		currComp =  myComp;
		currClassObjCollection = myComp.getCompCollection();
		currPosition = 0;
	}
	
	public boolean hasNext()
	{
//		System.out.println("Class Iterator.hasNext() -> position" + currPosition + ", size" + currClassObjCollection.size() );
		if(currPosition < currClassObjCollection.size())
			return true;
		return false;
	}
	public Object next()
	{
//		Object obj =  currComp.getItem(currPosition);
		Object obj =  currClassObjCollection.get(currPosition);		
		currPosition += 1;
//		System.out.println("Class Iterator.Next() after -> position" + currPosition + ", size" + currClassObjCollection.size() );

		return obj;
	}
	
	public boolean remove(Object obj)
	{
		if(currComp.removeItem((CFGComponent)obj))
		{
			currPosition -= 1;
			return true;
		}
		return false;
	}
	
	@Override
	public String toString()
	{
		return currComp.getItem(currPosition).toString();
	}

	@Override
	public void remove() {
		// TODO Auto-generated method stub
		
	}

}
