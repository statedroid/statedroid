package controller;

import java.io.File;
import java.util.ArrayList;

import models.cfg.APK;

import org.apache.log4j.Logger;

import analyzer.Analyzer;

import patternMatcher.attackreporter.AttackReporter;
import patternMatcher.events.csm.filereading.BufferedOutputStreamWriteEvent;
import taintanalyzer.TaintAnalyzer;
import configuration.Config;
import decompilation.APKDecompiler;

public class Controller {

	private static Logger logger = Logger.getLogger("TaintAnalyzer");

	public static void main(String args[]) {
		File inputFile = new File(args[0]);
		if (inputFile.isDirectory()) {
			for (File file : inputFile.listFiles()) {
				if (file.getAbsolutePath().endsWith(".apk")){
					apkAnalyzer(file);
				}
			}
		}else{
			if (inputFile.getAbsolutePath().endsWith(".apk"))
					apkAnalyzer(inputFile);
		}
	}

	public static void apkAnalyzer(File inputFile) {
		if (inputFile.getAbsolutePath().endsWith(".apk")
				) {
			CFGBuilder cfgBlrdr = new CFGBuilder();
			APK apk = cfgBlrdr.getAPKObject(inputFile);

			if (apk != null) {
				TaintAnalyzer a = new TaintAnalyzer();
				apk.accept(a);
				reportAllUniqueWarnings();
				logger.fatal("Analysis Finished for  " + inputFile.getAbsolutePath());
				System.out.println("Analysis Finished for  " + inputFile.getAbsolutePath());
				logger.fatal("CSM time = " + a.totalTimeTakenCSM );
				logger.fatal("ASM time = " + a.totalTimeTakenASM );
			}else{
				logger.fatal("Decompilation failed for this input file: " + inputFile.getName());
			}
		}
	}
	public static void reportAllUniqueWarnings(){
		Config.getInstance().resetDataForNewApp();
		AttackReporter.getInstance().resetAllExistingReports();

	}

}
